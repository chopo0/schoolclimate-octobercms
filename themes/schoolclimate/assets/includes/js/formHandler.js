$(function()
{
	var successMsg = "Your message has been sent."; // Message shown on success.
	var failMsg = "It seems that our mail server is not responding at this time. We apologize for the inconvenience!"; // Message shown on fail.

	$("input,textarea").jqBootstrapValidation(
    {
     	preventSubmit: true,
     	submitSuccess: function($form, event){
					if($form.attr('id') == 'contact-us-mail'){
						event.preventDefault();
						return false;
					}

					if(!$form.attr('action')) // Check form doesnt have action attribute
					{
						event.preventDefault(); // prevent default submit behaviour
		        var data = $form.serialize();
						var processorFile = window.location.origin+'/'+$form.attr('id');
						var formData = {};

						$form.find("input, textarea, option:selected").each(function(e) // Loop over form objects build data object
						{
							var fieldData =  $(this).val();
							var fieldID =  $(this).attr('id');

							if($(this).is(':checkbox')) // Handle Checkboxes
							{
								fieldData = $(this).is(":checked");
							}
							else if($(this).is(':radio')) // Handle Radios
							{
								fieldData = $(this).val()+' = '+$(this).is(":checked");
							}
							else if($(this).is('option:selected')) // Handle Option Selects
							{
								fieldID = $(this).parent().attr('id');
							}

							formData[fieldID] = fieldData;
						});

						$.ajax({
				        url: processorFile,
				    		type: "POST",
				    		data: data,
				    		cache: false,
				    		success: function() // Success
				 				{
									$form.append("<div id='form-alert'><div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><strong>"+successMsg+"</strong></div></div>");
				 	   		},
					   		error: function() // Fail
					   		{
									$form.append("<div id='form-alert'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><strong>"+failMsg+"</strong></div></div>");
					   		},
								complete: function() // Clear
								{
									$form.trigger("reset");
								},
				   		});
					}
      },
    	filter: function(){
			 	return $(this).is(":visible");
      },
	 });
});
