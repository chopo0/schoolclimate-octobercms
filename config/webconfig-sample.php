<?php

return [

    'APP_URL' => '//stagingcms.schoolclimate.org/',

    'DB_HOST' => '',
    'DB_NAME' => 'stagingcmsdb',
    'DB_USER_NAME' => '',
    'DB_PASSWORD' => '',

    'MAIL_DRIVER' => 'smtp',
    'MAIL_HOST' => 'smtp.gmail.com',
    'MAIL_PORT' => '587',
    'MAIL_FROM_ADDRESS' => '',
    'MAIL_FROM_NAME' => 'School Climate',
    'MAIL_USERNAME' => 'support@schoolclimate.org',
    'MAIL_PASSWORD' => '',
    'MAIL_ENCRYPTION' => 'tls',

    'STRIPE_KEY' => '',
    'STRIPE_SECRET' => 'sk_test_BXf82cP0i4mRkaPcebH2YLQM',

    'STRIPE_TEST_KEY' => '',
    'STRIPE_TEST_SECRET' => '',


    'CONSTANTCONTACT_CA' => '',
    'CONSTANTCONTACT_LIST' => '2088651115',
    'LINKPOLICY' => 'detect',

    'PAYPAL_API_CLIENT_ID' => '',
    'PAYPAL_API_SECRET' => '',
    'GOOGLE_CAPTCHA_SITE_KEY' => '',

];

?>
