<?php

return [

    'STRIPE_KEY' => config('webconfig.STRIPE_KEY'),
    'STRIPE_SECRET' => config('webconfig.STRIPE_SECRET'),

    'STRIPE_TEST_KEY' => config('webconfig.STRIPE_TEST_KEY'),
    'STRIPE_TEST_SECRET' => config('webconfig.STRIPE_TEST_SECRET'),
];

?>
