<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DbBackendAddFieldPDFInNewsLatterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('news_latters', function (Blueprint $table) {
				$table->string('pdf')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		  Schema::table('news_latters', function (Blueprint $table) {
		      $table->dropColumn('pdf');
		  });
	}

}
