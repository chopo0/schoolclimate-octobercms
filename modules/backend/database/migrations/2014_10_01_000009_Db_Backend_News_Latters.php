<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DbBackendNewsLatters extends Migration
{
    public function up()
    {
        Schema::create('news_latters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->integer('user_id');
            $table->longText('news_latter')->nullable();
            $table->longText('news_latter_image')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('news_latters');
    }
}
