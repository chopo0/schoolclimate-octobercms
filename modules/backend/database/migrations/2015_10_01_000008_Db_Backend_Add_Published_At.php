<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class DbBackendAddPublishedAt extends Migration
{
    public function up()
    {
        Schema::table('news_latters', function (Blueprint $table) {
            $table->timestamp('published_at');
        });

    }

    public function down()
    {
      if(Schema::hasColumn('news_latters', 'published_at'))
       {
         Schema::table('news_latters', function (Blueprint $table) {
             $table->dropColumn('published_at');
         });
       }
    }
}
