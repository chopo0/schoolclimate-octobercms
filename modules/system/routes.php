<?php

/**
 * Register System routes before all user routes.
 */
App::before(function ($request) {


  $user = BackendAuth::getUser();

  if($user){
    Event::listen('backend.menu.extendItems', function($navigationManager) use($user) {
      $groups = $user->groups->lists('id');
        if (count($groups) && !in_array(1, $groups)) {
               $navigationManager->removeMainMenuItem('October.System', 'system');
        }
    });
  }



    /*
     * Combine JavaScript and StyleSheet assets
     */
    Route::any('combine/{file}', 'System\Classes\Controller@combine');
});
