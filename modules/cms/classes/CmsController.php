<?php namespace Cms\Classes;

use App;
use Illuminate\Routing\Controller as ControllerBase;
use Closure;
use Input,Mail, Exception;

/**
 * The CMS controller class.
 * The base controller services front end pages.
 *
 * @package october\cms
 * @author Alexey Bobkov, Samuel Georges
 */
class CmsController extends ControllerBase
{
    use \October\Rain\Extension\ExtendableTrait;

    /**
     * @var array Behaviors implemented by this controller.
     */
    public $implement;

    /**
     * Instantiate a new CmsController instance.
     */
    public function __construct()
    {
        $this->extendableConstruct();
    }

    /**
     * Extend this object properties upon construction.
     */
    public static function extend(Closure $callback)
    {
        self::extendableExtendCallback($callback);
    }

    /**
     * Finds and serves the request using the primary controller.
     * @param string $url Specifies the requested page URL.
     * If the parameter is omitted, the current URL used.
     * @return string Returns the processed page content.
     */
    public function run($url = '/')
    {
        return App::make('Cms\Classes\Controller')->run($url);
    }

    public function sendMail(){

       $data = Input::all();
       $interest = strtolower($data['interest']);
       $toArray = [
                    'general information' => '',
                    'school climate survey (csci)' => '',
                    'school climate resource center' => '',
                    'professional development' => '',
                    'research' => 'research@schoolclimate.org',
                    'summer institute' => '',
                    'donations' => 'sgentile@schoolclimate.org',
                  ];


      if( $interest == 'donations' ||  $interest == 'research')
      {
        if(array_key_exists($interest, $toArray) && $toArray[$interest])
        {
          $to = $toArray[$interest];
          Mail::send(themes_path('schoolclimate.pages.emails.contact-us'), $data, function ($message) use ($data, $to) {
           $message->from($data['email'], 'NSCC');

           $message->to($to);
         });
        }

      }else{
        $this->ContactUsSalesforce($data);
      }


    //  $this->sendContactUsReply($data);

    }

    public function sendSCRCMail(){

       $data = Input::all();

       if(isset($data['message'])){
         $data['comment'] = $data['message'];
         unset($data['message']);
       }

       Mail::send(themes_path('schoolclimate.pages.emails.scrc-contact-us'), $data, function ($message) use ($data) {
        $message->from($data['email'], 'NSCC');

        $message->to('jgonzalez@schoolclimate.org');
      });

    }


    private function ContactUsSalesforce($data)
    {
        try{
            #Salesforce
            extract($data);

            //set POST variables
            $url = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
            $fields = array(
              'oid'=>urlencode($oid),
              'retURL'=>urlencode('http://www.schoolclimate.org/contact/index.php?success=true'),
              'first_name'=>urlencode($first_name),
              'last_name'=>urlencode($last_name),
              'email'=>urlencode($email),
              'phone'=>urlencode($phone),
              'company'=>urlencode($company),
              'city'=>urlencode($city),
              'state'=>urlencode($state),
              '00N50000001Qgbj'=>urlencode($interest),
               'lead_source'=>urlencode('CSEE Website'),
              '00N50000001Qhhx'=>urlencode($comments)
            );

            $fields_string = '';

            //url-ify the data for the POST
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }

            rtrim($fields_string,'&');

            //open connection
            $ch = curl_init();

           //set the url, number of POST vars, POST data
           curl_setopt($ch,CURLOPT_URL,$url);
           curl_setopt($ch,CURLOPT_POST,count($fields));
           curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

           //execute post
           $result = curl_exec($ch);

           //close connection
           curl_close($ch);
        }
        catch(Exception $e){
            return $e->getMessage();
        }
    }


    private function sendContactUsReply($data)
    {
        Mail::send(themes_path('schoolclimate.pages.emails.contact-us-reply'), $data, function ($message) use ($data) {
         $message->from('no-reply@schoolclimate.org', 'School Climate');

         $message->to($data['email']);
       });
    }
}
