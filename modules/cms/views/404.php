<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?= Lang::get('cms::lang.page.not_found.label') ?></title>
        <link href="<?= Url::asset('/modules/system/assets/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        <div class="container">
           <center style="margin-top:100px;">
			<h2>Opps! This page doesn't exist</h2>
            <p class="lead">Not to worry. You can either head back to <a href="/">homepage</a>.</p>
           </center> 
        </div>
    </body>
</html>
