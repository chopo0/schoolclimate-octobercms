<?php
use Digitalfox\Redirecturl\Models\RedirectUrl;

Route::post('contact-us-mail','Cms\Classes\CmsController@sendMail');
Route::post('join-learning-community','Cms\Classes\CmsController@sendSCRCMail');


/**
 * Register CMS routes before all user routes.
 */
App::before(function ($request) {

  $redirect =  RedirectUrl::where('active_url', '/'.$request->path())->first();

  if( $redirect )
    return redirect($redirect->current_url);

    $getCurrentPaht = ( $request->path() && $request->path() != '/' ) ? '/'. $request->path() : '';

    if( config('webconfig.LINKPOLICY') == 'secure' &&  isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO']  == 'http' )
    {

      if( $_SERVER['HTTP_HOST'] != 'www.schoolclimate.org' )
       return redirect('https://www.schoolclimate.org'. $getCurrentPaht);

      return redirect('https://www.schoolclimate.org'. $getCurrentPaht);
    }

    if( config('webconfig.LINKPOLICY') == 'secure' &&  $_SERVER['HTTP_HOST'] != 'www.schoolclimate.org')
     return redirect('https://www.schoolclimate.org'. $getCurrentPaht);

    /*
     * Extensibility
     */
    Event::fire('cms.beforeRoute');

    /*
     * The CMS module intercepts all URLs that were not
     * handled by the back-end modules.
     */
    Route::any('{slug}', 'Cms\Classes\CmsController@run')->where('slug', '(.*)?');



    /*
     * Extensibility
     */
    Event::fire('cms.route');
});
