<?php namespace Hambern\Featuredfiles;

use System\Classes\PluginBase;
use RainLab\Blog\Controllers\Posts as PostsController;
use RainLab\Blog\Models\Post as PostModel;

/**
 * Featuredfiles Plugin Information File
 */
class Plugin extends PluginBase
{
    
    /**
     * @var array   Require the RainLab.Blog plugin
     */
    public $require = ['RainLab.Blog'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'hambern.featuredfiles::lang.plugin.name',
            'description' => 'hambern.featuredfiles::lang.plugin.description',
            'author'      => 'Hambern',
            'icon'        => 'icon-file'
        ];
    }

    public function boot()
    {
        // Extend the model
        PostModel::extend(function ($model) {
            $model->attachMany['featured_files'] = [
                'System\Models\File',
                'order' => 'sort_order'
            ];
        });

        // Extend the controller
        PostsController::extendFormFields(function ($form, $model) {
            if (!$model instanceof PostModel) return;
            $form->addSecondaryTabFields([
                'featured_files' => [
                    'label'     => 'hambern.featuredfiles::lang.plugin.name',
                    'tab'       => 'rainlab.blog::lang.post.tab_manage',
                    'type'      => 'fileupload',
                    'mode'      => 'file',
                ]
            ]);
        });
    }
}
