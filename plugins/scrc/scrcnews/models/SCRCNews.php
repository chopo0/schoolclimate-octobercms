<?php namespace Scrc\ScrcNews\Models;

use Model;

/**
 * SCRCNews Model
 */
class SCRCNews extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'scrc_scrcnews_scrc_news';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
      'image' => ['System\Models\File']
    ];
    public $attachMany = [];

    public $rules = [
        'title' => 'required',
        // 'news_description' => 'required',
        'image' => 'required',
    ];

}
