<?php namespace Scrc\ScrcNews\Components;

use Cms\Classes\ComponentBase;
use Scrc\ScrcNews\Models\SCRCNews as SCRCNewsModel;

class SCRCNews extends ComponentBase
{

    public $scrcnews;

    public function componentDetails()
    {
        return [
            'name'        => 'SCRCNews Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun()
    {
        // $this->addJs('/plugins/scrc/scrcnews/assets/news-slider/jquery.bootstrap.newsbox.min.js');
        $this->scrcnews = $this->page['scrcnews'] = SCRCNewsModel::where('publish', 1)->orderBy('id','asc')->get();
    }
}
