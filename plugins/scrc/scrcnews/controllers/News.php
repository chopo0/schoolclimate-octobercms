<?php namespace Scrc\ScrcNews\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

use ApplicationException;
use Flash;
use Redirect;

use Scrc\ScrcNews\Models\SCRCNews;
/**
 * News Back-end Controller
 */
class News extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Scrc.ScrcNews', 'scrcnews', 'news');
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $postId) {
                if ((!$post = SCRCNews::find($postId))) {
                    continue;
                }

                $post->delete();
            }

            Flash::success('Successfully deleted!');
        }

        return $this->listRefresh();
    }

    public function index_onPublish()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

          SCRCNews::whereIn('id', $checkedIds)->update(['publish' => 1]);

            Flash::success('Successfully published!');
        }

        return $this->listRefresh();
    }

    public function index_onUnpublish()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

          SCRCNews::whereIn('id', $checkedIds)->update(['publish' => 0]);

            Flash::success('Successfully un-published!');
        }

        return $this->listRefresh();
    }

}
