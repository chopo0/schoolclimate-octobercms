<?php namespace Scrc\ScrcNews\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddPublishColumIntoSCRCNewsTable extends Migration
{

    public function up()
    {
        Schema::table('scrc_scrcnews_scrc_news', function($table)
        {
            $table->tinyInteger('publish')->default(0);
        });
    }

    public function down()
    {
      Schema::table('scrc_scrcnews_scrc_news', function($table)
      {
          $table->dropColumn('publish');
      });
    }

}
