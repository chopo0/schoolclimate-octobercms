<?php namespace Scrc\ScrcNews;

use Backend;
use System\Classes\PluginBase;

/**
 * scrcNews Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'scrcNews',
            'description' => 'No description provided yet...',
            'author'      => 'scrc',
            'icon'        => 'icon-newspaper-o'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Scrc\ScrcNews\Components\SCRCNews' => 'SCRCNews',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'scrc.scrcnews.some_permission' => [
                'tab' => 'SCRC News',
                'label' => 'Manage News'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'scrcnews' => [
                'label'       => 'SCRC News',
                'url'         => Backend::url('scrc/scrcnews/news'),
                'icon'        => 'icon-newspaper-o',
                'permissions' => ['scrc.scrcnews.*'],
                'order'       => 500,
            ],
        ];
    }

}
