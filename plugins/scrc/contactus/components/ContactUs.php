<?php namespace Scrc\ContactUs\Components;

use Cms\Classes\ComponentBase;
use Request, Validator, ValidationException, Mail;
use Scrc\ContactUs\Models\Contact;

class ContactUs extends ComponentBase
{
    public $interests;
    public $p_url;

    public function componentDetails()
    {
        return [
            'name'        => 'ContactUs Component',
            'description' => 'Contact Us Form'
        ];
    }

    public function defineProperties()
    {
      return [];
    }

    public function onRun()
    {
        $this->p_url = $this->page['p_url'] =  isset($_GET['p'])? $_GET['p']-1 : 0;
        $this->interests = $this->page['interests'] = [
                              'General Information',
                              'School Climate Survey (CSCI)',
                              'School Climate Resource Center',
                              'Professional Development',
                              'Research',
                              'Summer Institute',
                              'Donations',
                            ];
    }

    public function onHandleForm()
    {
      $request = Request::all();
      $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email',
        'phone' => 'required|regex:/^[0-9. -]+$/',
        'company' => 'required',
        'city' => 'required',
        'state' => 'required',
        'comments' => 'required',
        'g-recaptcha-response' => 'required',
      ];

      $validation = Validator::make(Request::all(), $rules);

      if ($validation->fails()) {
          throw new ValidationException($validation);
      }

      $data = $request;
      $savedata['first_name'] = $request['first_name'];
      $savedata['last_name'] = $request['last_name'];
      $savedata['email'] = $request['email'];
      $savedata['phone'] = $request['phone'];
      $savedata['city'] = $request['city'];
      $savedata['state'] = $request['state'];
      $savedata['comments'] = $request['comments'];
      $savedata['interest'] =  $request['interest'];
      Contact::create($savedata);
      
      $interest = strtolower($data['interest']);
      $toArray = [
                   'general information' => '',
                   'school climate survey (csci)' => '',
                   'school climate resource center' => '',
                   'professional development' => '',
                   'research' => 'research@schoolclimate.org',
                   'summer institute' => '',
                   'donations' => 'sgentile@schoolclimate.org',
                 ];


     if( $interest == 'donations' ||  $interest == 'research')
     {
       if(array_key_exists($interest, $toArray) && $toArray[$interest])
       {
         $to = $toArray[$interest];
         Mail::send(themes_path('schoolclimate.pages.emails.contact-us'), $data, function ($message) use ($data, $to) {
          $message->from($data['email'], 'NSCC');

          $message->to($to);
        });
       }

     }else{
       $this->ContactUsSalesforce($data);
     }


    }

    private function ContactUsSalesforce($data)
    {
        try{
            #Salesforce
            extract($data);

            //set POST variables
            $url = 'https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
            $fields = array(
              'oid'=>urlencode($oid),
              'retURL'=>urlencode('https://www.schoolclimate.org/about/contact-us'),
              'first_name'=>urlencode($first_name),
              'last_name'=>urlencode($last_name),
              'email'=>urlencode($email),
              'phone'=>urlencode($phone),
              'company'=>urlencode($company),
              'city'=>urlencode($city),
              'state'=>urlencode($state),
              '00N50000001Qgbj'=>urlencode($interest),
               'lead_source'=>urlencode('CSEE Website'),
              '00N50000001Qhhx'=>urlencode($comments),
              'debug' => '1',
              'debugEmail' => urlencode("jgonzalez@schoolclimate.org")
            );

            $fields_string = '';

            //url-ify the data for the POST
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }

            rtrim($fields_string,'&');

            //open connection
            $ch = curl_init();

           //set the url, number of POST vars, POST data
           curl_setopt($ch,CURLOPT_URL,$url);
           curl_setopt($ch,CURLOPT_POST,count($fields));
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
           curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

           //execute post
           curl_exec($ch);
           //close connection
           curl_close($ch);
        }
        catch(Exception $e){
           //$e->getMessage();
        }
    }


}
