<?php namespace Scrc\ContactUs\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Newsletters Back-end Controller
 */
class Newsletters extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Scrc.ContactUs', 'contactus', 'newsletters');
    }
}