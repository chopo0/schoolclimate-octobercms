<?php namespace Scrc\ContactUs\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Contacts Back-end Controller
 */
class Contacts extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Scrc.ContactUs', 'contactus', 'contacts');
    }

    public function onHandleForm()
    {
      $this->request =(object) Request::all();

      echo '<pre>'; print_r($this->request); die;

    }
}
