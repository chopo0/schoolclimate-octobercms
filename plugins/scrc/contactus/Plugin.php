<?php namespace Scrc\ContactUs;

use Backend;
use System\Classes\PluginBase;

/**
 * contactUs Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Contact Us',
            'description' => 'Contact Us & Newsletter Subscriber',
            'author'      => 'scrc',
            'icon'        => 'icon-envelope-o'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Scrc\ContactUs\Components\ContactUs' => 'ContactUs',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'scrc.contactus.some_permission' => [
                'tab' => 'contactUs',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'contactus' => [
                'label'       => 'Contact Us',
                'url'         => Backend::url('scrc/contactus/contacts'),
                'icon'        => 'icon-envelope-o',
                'permissions' => ['scrc.contactus.*'],
                'order'       => 500,
                'sideMenu' => [
                    'contacts' => [
                        'label'       => 'Contact Us',
                        'icon'        => 'icon-envelope-o',
                        'url'         => Backend::url('scrc/contactus/contacts'),
                        'permissions' => ['scrc.contactus.*'],
                    ],
                    // 'newsletters' => [
                    //     'label'       => 'Newsletter Subscribers',
                    //     'icon'        => 'icon-users',
                    //     'url'         => Backend::url('scrc/contactus/newsletters'),
                    //     'permissions' => ['scrc.contactus.*'],
                    // ],
            ],
            ],
        ];
    }

}
