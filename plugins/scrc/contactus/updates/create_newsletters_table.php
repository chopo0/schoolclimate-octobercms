<?php namespace Scrc\ContactUs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateNewslettersTable extends Migration
{

    public function up()
    {
        Schema::create('scrc_contactus_newsletters', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('scrc_contactus_newsletters');
    }

}
