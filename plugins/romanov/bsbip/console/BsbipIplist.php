<?php
namespace Romanov\Bsbip\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Romanov\Bsbip\Models\Ip as Ip;

class BsbipIplist extends Command
{
    protected $name = 'bsbip:iplist';
    protected $description = 'Backend secure by ip. Manage IPs';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        if($this->option('add')){
            $this->addIp($this->option('add'));
        }
        if($this->option('remove')){
            $this->remIp($this->option('remove'));
        }
        $bsbip = Ip::lists('ip', 'id');
        if(empty($bsbip)){
            $this->comment("IP list is empty");
        }else{
            $this->comment("ip address list");
            $this->output->writeln("---------------");
        }
        foreach($bsbip as $v){
            $this->output->writeln($v);
        }
    }

    private function addIp($ip_addr){
        $ip = new Ip();
        $ip->ip = $ip_addr;
        $ip->save();
        $this->info("IP address '$ip_addr' successfully added");
    }

    private function remIp($ip_addr){
        $ip = Ip::where('ip', '=', $ip_addr)->first();
        if($ip){
            $ip->delete();
            $this->info("IP address '$ip_addr' successfully deleted");
        }else{
            $this->error("Can't find ip '".$ip_addr."'");
        }
    }

    protected function getOptions()
    {
        return [
            ['add', null, InputOption::VALUE_REQUIRED, '', null],
            ['remove', null, InputOption::VALUE_REQUIRED, '', null],
        ];
    }
}