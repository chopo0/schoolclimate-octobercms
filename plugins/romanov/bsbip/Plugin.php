<?php namespace Romanov\Bsbip;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'romanov.bsbip::lang.plugin.name',
            'description' => 'romanov.bsbip::lang.plugin.description',
            'author'      => 'Alexander Romanov',
            'icon'        => 'icon-shield'
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'romanov.bsbip::lang.plugin.name',
                'description' => 'romanov.bsbip::lang.plugin.description',
                'icon'        => 'icon-shield',
                'class'       => '\Romanov\Bsbip\Models\Settings',
                'order'       => 1,
                'permissions' => ['romanov.bsbip.access'],
                'category'    => 'system::lang.system.categories.system'
            ],
        ];
    }

    public function registerPermissions()
    {
        return [
            'romanov.bsbip.access'  => ['tab' => 'system::lang.permissions.name', 'label' => 'romanov.bsbip::lang.plugin.permissions'],
        ];
    }

    public function register()
    {
        $this->registerConsoleCommand('bsbip.enable', 'Romanov\Bsbip\Console\BsbipEnable');
        $this->registerConsoleCommand('bsbip.disable', 'Romanov\Bsbip\Console\BsbipDisable');
        $this->registerConsoleCommand('bsbip.status', 'Romanov\Bsbip\Console\BsbipStatus');
        $this->registerConsoleCommand('bsbip.iplist', 'Romanov\Bsbip\Console\BsbipIplist');
    }

}
