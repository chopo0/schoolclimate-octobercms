<?php

App::before(function($request) {

    $bsbip = \Romanov\Bsbip\Models\Settings::get('is_enabled', false);
    if($bsbip){
        $bu = Config::get('cms.backendUri', 'backend');
        if($bu[0] == '/')   $bu = substr($bu, 1);

        if ($request->is($bu) || $request->is($bu.'/*'))
        {
            $ac = new \Romanov\Bsbip\Classes\AccessController($request->getClientIp());
            if($ac->is_access_deny()){
                return $ac->mode_run();
            }

        }
    }
});
