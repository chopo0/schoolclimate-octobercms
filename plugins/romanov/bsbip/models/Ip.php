<?php namespace Romanov\Bsbip\Models;

use Model;
use Lang;

\Validator::extend('foo', function($attribute, $value, $parameters) {
    return $value == 'foo';
});

class Ip extends Model
{
    public $table = 'romanov_bsbip_ips';

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'ip' => 'required|ip|unique:romanov_bsbip_ips',
    ];
    public $attributeNames;
    protected $guarded = ['*'];

    public function __construct()
    {
        $this->attributeNames = [
            'ip' => '"'.Lang::get('romanov.bsbip::lang.iplist.name').'"',
        ];
        parent::__construct();
    }

}