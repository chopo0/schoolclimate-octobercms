<?php namespace Romanov\Bsbip\Controllers;

use Lang;
use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;
use Romanov\Bsbip\Models\Ip as IpModel;

class Ips extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['romanov.bsbip.access'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('October.System', 'system', 'settings');
        SettingsManager::setContext('Romanov.Bsbip', 'settings');

        $this->addJs('/plugins/romanov/bsbip/assets/js/iplist.js');
    }

    public function onCreateForm()
    {
        $this->asExtension('FormController')->create();
        return $this->makePartial('create_form');
    }

    public function onCreate()
    {
        $this->asExtension('FormController')->create_onSave();
        return $this->listRefresh();
    }

    public function onUpdateForm()
    {
        $this->asExtension('FormController')->update(post('record_id'));
        $this->vars['recordId'] = post('record_id');
        return $this->makePartial('update_form');
    }

    public function onUpdate()
    {
        $this->asExtension('FormController')->update_onSave(post('record_id'));
        return $this->listRefresh();
    }

    public function onDelete()
    {
        $this->asExtension('FormController')->update_onDelete(post('record_id'));
        return $this->listRefresh();
    }

    public function index_onDeleteSelected()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $ipid) {
                if (!$ip = IpModel::find($ipid)) continue;
                $ip->delete();
            }
            Flash::success(Lang::get('romanov.bsbip::lang.iplist.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('romanov.bsbip::lang.iplist.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}