<?php namespace Creations\PayPal\Models;

use Model;

/**
 * PaymentExport Model
 */
class PaymentExport extends \Backend\Models\ExportModel
{
  public function exportData($columns, $sessionKey = null)
  {
      $request = $_REQUEST;
      $contitions = [];

      $date_range = isset($request['date_range'])?$request['date_range']:'';
      $date_range = explode(' - ', $date_range);

      $on_behalf = isset($request['on_behalf'])?$request['on_behalf']:0;
      $in_honor = isset($request['in_honor'])?$request['in_honor']:0;
      $in_memory = isset($request['in_memory'])?$request['in_memory']:0;

      if($on_behalf) $contitions['on_behalf'] = $on_behalf;
      if($in_honor) $contitions['in_honor'] = $in_honor;
      if($in_memory) $contitions['in_memory'] = $in_memory;

      $payments = new Payment;

      $payments = $payments->whereBetween('created_at', $date_range);

      $i=0;
      if(count($contitions)){
        foreach($contitions as $in => $va){
          if($i)
            $payments = $payments->orWhere($in,$va);
          else
            $payments = $payments->where($in,$va);

          $i++;
        }
      }

      $payments = $payments->get();

      $payments->each(function($payment) use ($columns) {
          $arr = [];
          if($payment->on_behalf == 1)
            $arr[] = 'On behalf of';
          if($payment->in_honor == 1)
            $arr[] = 'In Honor';
          if($payment->in_memory == 1)
            $arr[] = 'In memory of';

            $payment->recipient_name = implode(' / ', $arr).' :'.$payment->recipient_name;

          $payment->addVisible($columns);
      });

      return $payments->toArray();
  }
}
