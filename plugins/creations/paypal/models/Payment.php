<?php namespace Creations\PayPal\Models;

use Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Payment Model
 */
class Payment extends Model
{
    use SoftDeletes;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'creations_paypal_payments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getFullAddressAttribute() {
        $return = $this->address_1;

        if($this->address_1)
          $return .= ', ';

        if($this->address_2)
          $return .= $this->address_2.', <br>';

        if($this->city)
          $return .= '<b>City: </b>'.$this->city.',<br>';

        if($this->state)
          $return .= '<b>State: </b>'.$this->state.',<br>';

        if($this->country_code)
          $return .= '<b>Country: </b>'.$this->country_code.',<br>';

        if($this->zip)
          $return .= '<b>Zip: </b>'.$this->zip;

        return $return;
    }

    public function getDonationTypeAttribute()
    {
        $type = 'N/A';

        if($this->on_behalf)
          $type = 'On behalf of: ';
        elseif($this->in_honor)
          $type = 'In Honor: ';
        elseif($this->in_memory)
          $type = 'In memory of: ';

        if($this->recipient_name)
          $type .= $this->recipient_name;

        return $type;
    }


    public function getContactInfoAttribute()
    {
        $contact = '';

        if($this->email)
          $contact .= '<b>Email: </b>'.$this->email.'<br>';

        if($this->phone)
          $contact .= '<b>Phone: </b>'.$this->phone;


        return $contact;
    }
}
