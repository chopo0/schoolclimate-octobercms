<?php namespace Creations\PayPal\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePaymentExportsTable extends Migration
{

    public function up()
    {
        Schema::create('creations_paypal_payment_exports', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('creations_paypal_payment_exports');
    }

}
