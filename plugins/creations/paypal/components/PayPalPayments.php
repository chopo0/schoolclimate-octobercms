<?php namespace Creations\PayPal\Components;

use Cms\Classes\ComponentBase;

use Request;
use Mail;
use Session;
use Redirect;
use Config;

use Creations\PayPal\Models\Payment as PayPalPayment;
use Creations\PayPal\Models\Plan  as PayPalPlan;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Common\PayPalModel;

use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;

use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;

use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Item;
use PayPal\Api\PaymentExecution;

use PayPal\Api\ShippingAddress;

use PayPal\Api\Webhook;
use PayPal\Api\WebhookEventType;

use PayPal\Exception\PayPalConnectionException;

use DB;


class PayPalPayments extends ComponentBase
{

    public $apiContext;
    protected $request, $amount, $plan, $subscription, $error, $interval;
    public $activeUrl;

    function __construct()
    {
      $this->activeUrl  = '/'.Request::path();
    }

    public function componentDetails()
    {
        return [
            'name'        => 'PayPal Payments Component',
            'description' => 'Pay with PayPal'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if(isset($_REQUEST['paymentId'])){
            $this->executePayment((object) Request::all());
            return redirect($this->activeUrl);
        }
        elseif(isset($_REQUEST['token']) && !isset($_REQUEST['paymentId'])){
            $this->executeAgreement($_REQUEST['token']);
            return redirect($this->activeUrl);
        }

        $this->addJs('/plugins/creations/paypal/assets/js/paypal_payments.js');

    }

    public function onHandleForm()
    {
      $this->request =(object) Request::all();
      $amt = 0;

      if($this->request->donationAmountOther && is_numeric($this->request->donationAmountOther))
        $amt = $this->request->donationAmountOther;
      elseif(isset($this->request->donationAmountSelected) && is_numeric($this->request->donationAmountSelected))
        $amt = $this->request->donationAmountSelected;
      else{
        Session::flash('error', 'Please select or add donation amount!');
        return back();
      }

      $this->amount = $this->page['amount'] = $amt;
      $this->interval = isset($this->request->donation_frequency)? $this->request->donation_frequency : '';

      Session::put('payment_data', $this->request);
      Session::put('payment_amount', $this->amount);

      if($this->interval){

        $checkPlan = PayPalPlan::where(['amount' => $this->amount, 'currency' => 'USD', 'frequency' => $this->interval])->first();

        if($checkPlan){
            $plan = $this->getPlan($checkPlan->plan_id);
            // $this->activatePlan($plan,'ACTIVE');

            $approveUrl = $this->createAgreement($plan);

            $payUrl =  $approveUrl;
        }
        else{
            $planDetails = (object) [
                                        'title' => 'Donation $'.$this->amount,
                                        'description' => 'Monthly donation plan of '.$this->amount,
                                        'frequency' => $this->interval,
                                        'frequencyInterval' => 1,
                                        'price' => $this->amount,
                                        'price_currency' => 'USD',
                                        'returnUrl' => url($this->activeUrl),
                                        'cancelUrl' => url($this->activeUrl),
                                    ];

            $payUrl =  $this->createPlan($planDetails);
        }
      }
      else
        $payUrl =  $this->singlePayment();

      return redirect($payUrl);
    }

    private function getPlan($id)
    {
      try {
        $plan = Plan::get($id, $this->apiContext());

      } catch (Exception $ex) {
        echo $ex->getMessage(); die;
      }
      return $plan;
    }

    private function apiContext()
    {
        return new ApiContext(
                new OAuthTokenCredential(
                    Config::get('webconfig.PAYPAL_API_CLIENT_ID'),     // ClientID
                    Config::get('webconfig.PAYPAL_API_SECRET')      // ClientSecret
                )
        );

    }

    private function singlePayment ()
    {

        $apiContext = $this->apiContext();

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");


        $item1 = new Item();
        $item1->setName('Donation')
            ->setCurrency('USD')
            ->setQuantity(1)
            // ->setSku("123123") // Similar to `item_number` in Classic API
            ->setPrice($this->amount);


        $amount = new Amount();
        $amount->setTotal($this->amount);
        $amount->setCurrency('USD');

        $transaction = new Transaction();
        $transaction->setAmount($amount);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url($this->activeUrl))
                     ->setCancelUrl(url($this->activeUrl));

         $payment = new Payment();
         $payment->setIntent('sale')
                 ->setPayer($payer)
                 ->setTransactions(array($transaction))
                 ->setRedirectUrls($redirectUrls);

         try {
              $payment->create($apiContext);
         }
         catch (\PayPal\Exception\PayPalConnectionException $ex) {
             // This will print the detailed information on the exception.
             //REALLY HELPFUL FOR DEBUGGING
             echo $ex->getData();

             echo $ex->getMessage(); die;
         }

         return $payment->getApprovalLink();

    }

    private function executePayment($request)
    {
        $paymentdata = Session::get('payment_data');
        $amt = Session::get('payment_amount');


        $paymentId = $request->paymentId;
        $payment = Payment::get($paymentId, $this->apiContext());

        $execution = new PaymentExecution();
        $execution->setPayerId($request->PayerID);

        $transaction = new Transaction();
        $amount = new Amount();

        $amount->setCurrency('USD');
        $amount->setTotal($amt);
        // $amount->setDetails('Donation');
        $transaction->setAmount($amount);
        $execution->addTransaction($transaction);


        try {
            $result = $payment->execute($execution, $this->apiContext());
            try {
                $payment = Payment::get($paymentId, $this->apiContext());
            } catch (Exception $ex) {
                echo $ex->getMessage(); die;
            }
        } catch (Exception $ex) {
            echo $ex->getMessage(); die;
        }

        $this->storePaymentDetails($payment);
    }

    private function storePaymentDetails($payment, $type=null)
    {
        $successMess = ' donation of <b>$'.Session::get('payment_amount').'</b>';
        $payment_data = Session::get('payment_data');
        $phone = ($payment_data && $payment_data->phone)?$payment_data->phone:$payment->payer->payer_info->phone;

        $data = [
                  'payment_id' => $payment->id,
                  'first_name' => ($payment_data && $payment_data->first_name)?$payment_data->first_name:$payment->payer->payer_info->first_name,
                  'last_name' => ($payment_data && $payment_data->last_name)?$payment_data->last_name:$payment->payer->payer_info->last_name,
                  'payer_id' => $payment->payer->payer_info->payer_id,
                  'email' => ($payment_data && $payment_data->email)?$payment_data->email:$payment->payer->payer_info->email,
                  'phone' => ($phone)?$phone:'',
                  'country_code' => $payment->payer->payer_info->country_code,
                  'amount' => Session::get('payment_amount'),
                  'address_1' => ($payment_data && $payment_data->address_1)?$payment_data->address_1:$payment->payer->payer_info->shipping_address->line1,
                  'address_2' => ($payment_data && $payment_data->address_2)?$payment_data->address_2:'',
                  'city' => ($payment_data && $payment_data->city)?$payment_data->city:$payment->payer->payer_info->shipping_address->city,
                  'state' => ($payment_data && $payment_data->state)?$payment_data->state:$payment->payer->payer_info->shipping_address->state,
                  'zip' => ($payment_data && $payment_data->zip)?$payment_data->zip:$payment->payer->payer_info->shipping_address->postal_code,
                  'on_behalf' => ($payment_data && isset($payment_data->onBehalf))?$payment_data->onBehalf:0,
                  'in_honor' => ($payment_data && isset($payment_data->inHonor))?$payment_data->inHonor:0,
                  'in_memory' => ($payment_data && isset($payment_data->inMemory))?$payment_data->inMemory:0,
                  'recipient_name' => ($payment_data && isset($payment_data->recipientName))?$payment_data->recipientName:'',
                ];

        if($type == 'agmnt'){
          $data['plan'] = Session::get('planId');
          $data['country_code'] = $payment->payer->payer_info->shipping_address->country_code;
          $successMess = ' Monthly donation of <b>$'.Session::get('payment_amount').'</b>';
        }
        else
          $data['plan'] = '-';

        try{
          PayPalPayment::create($data);

          if(isset($payment_data->newslatterSubscribe ))
           {
             $this->getEmailSubscribed($data['email']);
           }
        }
        catch(Exception $e){
          echo $e->getMessage(); die;
        }

        $this->buyerEmail();
        $this->developmentEmail();

        Session::forget('payment_amount');
        Session::forget('payment_data');
        Session::forget('planId');

        Session::flash('success-donate', $successMess);

        return;
    }

    private function createPlan($details)
    {
      $plan = new Plan();
      $plan->setName($details->title)->setDescription($details->description)->setType('INFINITE');

      $paymentDefinition = new PaymentDefinition();
      $paymentDefinition->setName('Regular Payments')
                        ->setType('REGULAR')
                        ->setFrequency($details->frequency)
                        ->setFrequencyInterval($details->frequencyInterval)
                        // ->setCycles('12')
                        ->setAmount(new Currency(array('value' => $details->price, 'currency' => $details->price_currency)));


      $merchantPreferences = new MerchantPreferences();
      $merchantPreferences->setReturnUrl($details->returnUrl)
                          ->setCancelUrl($details->cancelUrl)
                          ->setAutoBillAmount("yes")
                          ->setInitialFailAmountAction("CONTINUE")
                          ->setMaxFailAttempts("0");

      $plan->setPaymentDefinitions(array($paymentDefinition));
      $plan->setMerchantPreferences($merchantPreferences);

      $request = clone $plan;

      try {
        $output = $plan->create($this->apiContext());
        try{
          $this->activatePlan($plan,'ACTIVE');
        }
        catch(Exception $ex){
          echo $ex->getMessage(); die;
        }

        $approveUrl = $this->createAgreement($plan);
        return $approveUrl;

      } catch (Exception $ex) {
         echo $ex->getMessage(); die;
      }
    }

    private function storePlan($plan)
    {
        $data = [
                    'plan_id' => $plan->getId(),
                    'name' => $plan->name,
                    'type' => $plan->payment_definitions[0]->type,
                    'frequency' => $plan->payment_definitions[0]->frequency,
                    'amount' => $plan->payment_definitions[0]->amount->value,
                    'currency' => $plan->payment_definitions[0]->amount->currency,
                    'cycles' => $plan->payment_definitions[0]->cycles,
                    'frequency_interval' => $plan->payment_definitions[0]->frequency_interval,
                ];

        try{
          PayPalPlan::create($data);
        }
        catch(Exception $ex){
          echo $ex->getMessage();
        }

    }

    private function activatePlan($plan,$state)
    {
      try {
          $patch = new Patch();

          $value = new PayPalModel('{
                 "state":"'.$state.'"
               }');

          $patch->setOp('replace')
                ->setPath('/')
                ->setValue($value);

          $patchRequest = new PatchRequest();
          $patchRequest->addPatch($patch);

          $plan->update($patchRequest, $this->apiContext());

          $updatedPlan =  Plan::get($plan->getId(), $this->apiContext());

          $this->storePlan($plan);

      } catch (Exception $ex) {
         echo $ex->getMessage(); die;
      }

    }

    private function createAgreement($plan)
    {
      Session::put('planId', $plan->getId());

      $date = rtrim(date('c', strtotime('+5 minutes', time())),'+00:00').'Z';

      $agreement = new Agreement();
      $agreement->setName($plan->name)
                ->setDescription($plan->description)
                ->setStartDate($date);

      $newPlan = new Plan();
      $newPlan->setId($plan->id);
      $agreement->setPlan($newPlan);

      $payer = new Payer();
      $payer->setPaymentMethod('paypal');
      $agreement->setPayer($payer);

      $request = clone $agreement;

      try {
        $agreement = $agreement->create($this->apiContext());

        return $agreement->getApprovalLink();

      }
      catch (Exception $ex) {
          echo $ex->getMessage(); die;
      }
    }

    private function executeAgreement($token)
    {
        $agreement = new Agreement();
        try {
            $agreement->execute($token, $this->apiContext());

            $this->storePaymentDetails($agreement, 'agmnt');

            return $agreement;
        } catch (Exception $ex) {
            exit(1);
        }
    }

    private function suspendAgreement ($agreement_id)
    {
      $createdAgreement = Agreement::get($agreement_id, $this->apiContext());

      //Create an Agreement State Descriptor, explaining the reason to suspend.
      $agreementStateDescriptor = new AgreementStateDescriptor();
      $agreementStateDescriptor->setNote("Suspending the agreement");

      try {
          $createdAgreement->suspend($agreementStateDescriptor, $this->apiContext());
          $agreement = Agreement::get($createdAgreement->getId(), $this->apiContext());
           \Log::info('#Suspend Agreement - '.$agreement);
          return $agreement;

      }
       catch(PayPalConnectionException $ex){
         exit(1);
       }
       catch (Exception $ex) {
         exit(1);
      }
    }

    private function buyerEmail()
    {
       try{

           $this->request = Session::get('payment_data');
          $this->interval = isset($this->request->donation_frequency)? $this->request->donation_frequency : '';
          $this->amount =  Session::get('payment_amount');

          $intervalTest = '';

          if( $this->interval == 'month')
          $intervalTest = 'Monthly';

          if( $this->interval == '3-month')
          $intervalTest = 'Quarterly';

          if( $this->interval == 'year')
          $intervalTest = 'Annual';

          $data = [
                    'amount'=> $this->amount,
                    'url'=> Config::get('app.url'),
                    'date' => date('F d, Y'),
                    'day' => date('jS'),
                    'monthly' => $intervalTest
                  ];

        if(isset($this->request->recipientName) && $this->request->recipientName){

            $data['recipientName'] = $this->request->recipientName;
            $data['onBehalf'] = '';
            $data['inHonor'] = '';
            $data['inMemory'] = '';

            if(isset($this->request->onBehalf) && $this->request->onBehalf)
              $data['onBehalf'] = ' on behalf of ';
            if(isset($this->request->inHonor) && $this->request->inHonor)
              $data['inHonor'] = ' In Honor ';
            if(isset($this->request->inMemory) && $this->request->inMemory)
              $data['inMemory'] = ' In memory of ';

            Mail::send('digitalfox.stripe::mail.recipient_email',$data, function ($message)
            {
             $message->subject('Donation Confirmed');
             $message->to($this->request->email);
            });
        }
        else{
            Mail::send('digitalfox.stripe::mail.buyer_email',$data, function ($message)
            {
             $message->subject('Donation Confirmed');
             $message->to($this->request->email);
            });
        }
      }catch(\Exception $e){

      }


    }

    private function developmentEmail()
    {
        $this->request = Session::get('payment_data');
        $this->interval = isset($this->request->donation_frequency)? $this->request->donation_frequency : '';
        $this->amount =  Session::get('payment_amount');

        $developementTeam = DB::table('backend_users')->join('backend_users_groups', 'backend_users_groups.user_id', '=', 'backend_users.id')
                                                      ->join('backend_user_groups', 'backend_user_groups.id', '=', 'backend_users_groups.user_group_id')
                                                      ->where('backend_user_groups.code', 'development')
                                                      ->lists('backend_users.email');

        if(!count($developementTeam)) return;

        $intervalTest = '';

        if( $this->interval == 'month')
        $intervalTest = 'Monthly';

        if( $this->interval == '3-month')
        $intervalTest = 'Quarterly';

        if( $this->interval == 'year')
        $intervalTest = 'Annual';

        $data = [
                  'amount'=> $this->amount,
                  'url'=> Config::get('app.url'),
                  'date' => date('F d, Y'),
                  'day' => date('jS'),
                  'monthly' => $intervalTest,
                  'donorEmail' => $this->request->email,
                  'donorName' => $this->request->first_name.' '.$this->request->last_name,
                ];


        foreach($developementTeam as $team){

          Mail::send('digitalfox.stripe::mail.development_email',$data, function ($message) use($team)
          {
           $message->subject('Donation Revieved');
           $message->to($team);
          });

        }
    }


    private function getEmailSubscribed($email)
    {
            try{

            $data =  [
              'ca' => Config::get('constantcontact.CONSTANTCONTACT_CA'),
              'list' => Config::get('constantcontact.CONSTANTCONTACT_LIST'),
              'source' => 'EFD',
              'required' => 'list,email',
              'url' => '/newslatter-response',
              'email' => $email
            ];

            $url = 'https://visitor2.constantcontact.com/api/signup';
            $handle = curl_init($url);
            curl_setopt($handle, CURLOPT_POST, true);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
            $response = curl_exec($handle);
            curl_close($handle);
          }catch(\Exception $e){
            if($e->getMessage() != 'Plan already exists.')
            {
              \Log::info($e->getMessage());
            }
          }
    }

}
