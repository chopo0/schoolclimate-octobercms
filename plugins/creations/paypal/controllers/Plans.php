<?php namespace Creations\PayPal\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

use Creations\PayPal\Models\Plan as PlanModel;

use ApplicationException;
use Flash;
use Redirect;

/**
 * Plans Back-end Controller
 */
class Plans extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Creations.PayPal', 'paypal', 'plans');
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $postId) {
                if ((!$plan = PlanModel::find($postId))) {
                    continue;
                }

                $plan->delete();
            }

            Flash::success('Successfully deleted those team members.');
        }

        return $this->listRefresh();
    }
}
