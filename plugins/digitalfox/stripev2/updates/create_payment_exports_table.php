<?php namespace Digitalfox\Stripev2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePaymentExportsTable extends Migration
{

    public function up()
    {
        Schema::create('digitalfox_stripe_payment_exports', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('digitalfox_stripe_payment_exports');
    }

}
