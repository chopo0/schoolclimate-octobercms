$(document).ready(function () {
  $('input[name=makeMonthly] , input[name=newslatterSubscribe], input[name=donation_frequency],input[name=onBehalf],input[name=inHonor],input[name=inMemory],input[name=onBehalfHonor]').iCheck({
    checkboxClass: 'icheckbox_square-orange',
    radioClass: 'iradio_square-orange',
    increaseArea: '20%' // optional
  });


  $('input[type="checkbox"][name="onBehalf"], input[type="checkbox"][name="inHonor"], input[type="checkbox"][name="inMemory"]').on('ifChanged', function (event) {
    if (!$(this).parent('div').hasClass('checked')) {
      $('.recipientDiv').show();
      $('#recipientName').addClass('required');
    } else if ($('input[type="checkbox"][name="onBehalf"]').parent('div').hasClass('checked') && ($('input[type="checkbox"][name="inHonor"]').parent('div').hasClass('checked') || $('input[type="checkbox"][name="inMemory"]').parent('div').hasClass('checked'))) {

    } else if ($('input[type="checkbox"][name="inHonor"]').parent('div').hasClass('checked') && ($('input[type="checkbox"][name="onBehalf"]').parent('div').hasClass('checked') || $('input[type="checkbox"][name="inMemory"]').parent('div').hasClass('checked'))) {

    } else if ($('input[type="checkbox"][name="inMemory"]').parent('div').hasClass('checked') && ($('input[type="checkbox"][name="inHonor"]').parent('div').hasClass('checked') || $('input[type="checkbox"][name="onBehalf"]').parent('div').hasClass('checked'))) {

    } else {
      $('#recipientName').removeClass('required');
      $('.recipientDiv').hide();
    }
  });


  $('.amountOption-btn input:checked').parent().removeClass('btn-amount').addClass('btn-success');

  $('.amountOption-btn').click(function () {
    $('.amountOption-btn input').parent().removeClass('btn-success').addClass('btn-amount');
    $('.amountOption-btn').find('input').prop('checked', false);
    $(this).find('input').prop('checked', true);

    if ($('.amountOption-btn input:checked').val() == 'other') {
      $('#donationAmountOther').val('');
      $('#donationAmountOther').attr('required', 'required');
    } else {
      $('#donationAmountOther').removeAttr('required');
    }

    $('.amountOption-btn input:checked').parent().removeClass('btn-amount').addClass('btn-success');
  });

});

function removeMapClasses(cycle, r_class) {
  $(cycle).map(function () {
    if ($(this).hasClass(r_class)) {
      $(this).removeClass(r_class);
    }
  });
}