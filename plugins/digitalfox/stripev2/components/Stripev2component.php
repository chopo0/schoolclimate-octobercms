<?php namespace Digitalfox\Stripev2\Components;

use config;
use Request;
use Mail;
use Session;
use Redirect;
use Cms\Classes\ComponentBase;
use Digitalfox\Stripev2\Models\Payment;
use Digitalfox\Stripev2\Models\Customer;
use Digitalfox\Stripev2\Models\Plan;
use Digitalfox\Stripev2\Models\Subscription;
use DB;

class Stripev2component extends ComponentBase
{
  protected $customer, $oldCustomer, $request, $amount, $plan, $subscription, $error, $interval;

  public function componentDetails()
  {
    return [
      'name' => 'stripev2component Component',
      'description' => 'No description provided yet...'
    ];
  }

  public function defineProperties()
  {
    return [];
  }

  public function onRun()
  {
    $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $url = explode('/', trim($url, '/'));
    $segment1 = $url[0];
    $this->page['donation_amount'] = [50, 100, 200, 500, 1000, 'other'];

    if ($segment1 == 'copies')
      $this->page['STRIPE_KEY'] = config::get('stripe.STRIPE_TEST_KEY');
    else
      $this->page['STRIPE_KEY'] = config::get('stripe.STRIPE_KEY');

    $this->page['session'] = Session::get('success-donate');
    $this->page['error'] = Session::get('error');
    $this->page['stripeError'] = Session::get('stripe-error');

    $this->addCss('/plugins/digitalfox/stripev2/assets/css/donation.css');
    $this->addCss('/plugins/digitalfox/stripev2/assets/css/donation-custom.css');
    $this->addCss('//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/orange.png');
    $this->addCss('//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css');

    $this->addJs('//ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js');
    $this->addJs('/plugins/digitalfox/stripev2/assets/js/jquery.creditCardValidator.js');
    $this->addJs('https://js.stripe.com/v1/');
    $this->addJs('/plugins/digitalfox/stripev2/assets/js/payment.js');
    $this->addJs('//cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.js');
    $this->addJs('/plugins/digitalfox/stripev2/assets/js/donation_custom.js');

  }

  public function onHandleForm()
  {
    $this->request = (object)Request::all();

    $amt = 0;

    if ($this->request->donationAmountOther && is_numeric($this->request->donationAmountOther))
      $amt = $this->request->donationAmountOther;
    elseif (isset($this->request->donationAmountSelected) && is_numeric($this->request->donationAmountSelected))
      $amt = $this->request->donationAmountSelected;
    else {
      Session::flash('error', 'Please select or add donation amount!');

        // $this->buyerEmail();

      return back();
    }

    $this->amount = $this->page['amount'] = $amt;
    $this->interval = isset($this->request->donation_frequency) ? $this->request->donation_frequency : '';

    if (isset($this->request->paypal)) {
      $successMess = ' donation of <b>$' . $this->amount . '</b>';

      if ($this->interval)
        $successMess = ' Monthly donation of <b>$' . $this->amount . '</b>';

      Session::flash('success-donate', $successMess);

      $this->buyerEmail();

      return back();
    } else {
      $this->stripePayment();
    }


    if ($this->error) return Redirect::back();
  }


  private function stripePayment()
  {
    try {
      $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
      $url = explode('/', trim($url, '/'));
      $segment1 = $url[0];

      if ($segment1 == 'copies')
        \Stripe\Stripe::setApiKey(config::get('stripe.STRIPE_TEST_SECRET'));
      else
        \Stripe\Stripe::setApiKey(config::get('stripe.STRIPE_SECRET'));

      //Stripe process donation
      if ($this->interval)
        $response = (object)$this->makeRecurringDonation();
      else
        $response = (object)$this->makeSingleDonation();

      if (isset($this->request->newslatterSubscribe)) {
        $this->getEmailSubscribed();
      }

      $this->makeCustomer();

      Payment::storeCharge($this->request, $this->amount, $this->customer, $response);

      $successMess = ' donation of <b>$' . $this->amount . '</b>';

      if ($this->interval)
        $successMess = ' Monthly donation of <b>$' . $this->amount . '</b>';

      Session::flash('success-donate', $successMess);

      $this->buyerEmail();
      $this->developmentEmail();

      return back();

    } catch (\Stripe\Error\Card $e) {
          // Since it's a decline, \Stripe\Error\Card will be caught
      $this->exceptionHandler($e);

    } catch (\Stripe\Error\RateLimit $e) {
          // Too many requests made to the API too quickly
      $this->exceptionHandler($e);
    } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API
      $this->exceptionHandler($e);
    } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed
          // (maybe you changed API keys recently)
      $this->exceptionHandler($e);
    } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed
      $this->exceptionHandler($e);
    } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
          // yourself an email
      $this->exceptionHandler($e);
    } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
      $this->exceptionHandler($e);
    }
  }

  private function exceptionHandler($e)
  {
    $body = $e->getJsonBody();

      //$body['error']['message'];
    Session::flash('stripe-error', ' Make sure you have entered your credit card information correctly - If you continue to receive errors, try using a different credit card or please contact your financial institution.');
    $this->error = true;
  }


  private function makeSingleDonation()
  {
    $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $url = explode('/', trim($url, '/'));
    $segment1 = $url[0];

    if ($segment1 == 'copies')
      \Stripe\Stripe::setApiKey(config::get('stripe.STRIPE_TEST_SECRET'));
    else
      \Stripe\Stripe::setApiKey(config::get('stripe.STRIPE_SECRET'));
    $paymentDesc = $this->request->first_name . " " . $this->request->last_name . ' contribute donation amount $' . $this->amount;

    if ($this->request->address_1)
      $paymentDesc .= ',# ' . $this->request->address_1 . ', ';

    // if ($this->request->address_2)
    //   $paymentDesc .= $this->request->address_2 . ', ';

    if ($this->request->city)
      $paymentDesc .= 'City: ' . $this->request->city . ', ';

    if ($this->request->state)
      $paymentDesc .= 'State: ' . $this->request->state . ', ';

    if ($this->request->zip)
      $paymentDesc .= 'Zip: ' . $this->request->zip;

    if (isset($this->request->recipientName) && $this->request->recipientName) {
      if (isset($this->request->onBehalf) && $this->request->onBehalf)
        $paymentDesc .= ', on behalf of';
      elseif (isset($this->request->inHonor) && $this->request->inHonor)
        $paymentDesc .= ', in Honor';
      elseif (isset($this->request->inMemory) && $this->request->inMemory)
        $paymentDesc .= ', in memory of';

      $paymentDesc .= ' ' . $this->request->recipientName;
    }
    if (isset($this->request->onBehalfHonor)) {
      $paymentDesc .= ', on behalf or honor of ' . $this->request->recipient_first_name . ' ' . $this->request->recipient_last_name . '(' . $this->request->recipient_email . ')';
    }
    return \Stripe\Charge::create(array(
      "amount" => $this->amount * 100,
      "currency" => 'usd',
      "source" => $this->request->stripeToken, // obtained with Stripe.js
      "description" => $paymentDesc,
    ));
  }


  private function makeRecurringDonation()
  {
    $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $url = explode('/', trim($url, '/'));
    $segment1 = $url[0];

    if ($segment1 == 'copies')
      \Stripe\Stripe::setApiKey(config::get('stripe.STRIPE_TEST_SECRET'));
    else
      \Stripe\Stripe::setApiKey(config::get('stripe.STRIPE_SECRET'));

    $customer = \Stripe\Customer::create(array(
      "email" => $this->request->email,
    ));

    $totalamount = $this->amount * 100;
    $stripePlanId = $this->createStripePlan();

    if ($stripePlanId) {
      return \Stripe\Subscription::create(
        [
          "customer" => $customer->id,
          "plan" => $stripePlanId,
          "source" => $this->request->stripeToken,
        ]
      );
    }

  }

  private function createStripePlan()
  {
    $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $url = explode('/', trim($url, '/'));
    $segment1 = $url[0];

    if ($segment1 == 'copies')
      \Stripe\Stripe::setApiKey(config::get('stripe.STRIPE_TEST_SECRET'));
    else
      \Stripe\Stripe::setApiKey(config::get('stripe.STRIPE_SECRET'));

    $stripePlanId = "donation-" . $this->interval . '-' . $this->amount;

    try {
                 //Create Stripe Plans
      \Stripe\Plan::create(array(
        "name" => $stripePlanId,
        "id" => $stripePlanId,
        "interval" => $this->interval,
        "currency" => 'usd',
        "amount" => ($this->amount * 100),
      ));
    } catch (\Exception $e) {
      if ($e->getMessage() != 'Plan already exists.') {
        \Log::info($e->getMessage());
        die;
      }
    }

    return $stripePlanId;
  }


  private function buyerEmail()
  {
    $intervalTest = '';

    if ($this->interval == 'month')
      $intervalTest = 'Monthly';

    if ($this->interval == '3-month')
      $intervalTest = 'Quarterly';

    if ($this->interval == 'year')
      $intervalTest = 'Annual';

    $data = [
      'amount' => $this->amount,
      'url' => Config::get('app.url'),
      'date' => date('F d, Y'),
      'day' => date('jS'),
      'monthly' => $intervalTest
    ];

    if (isset($this->request->recipientName) && $this->request->recipientName) {

      $data['recipientName'] = $this->request->recipientName;
      $data['onBehalf'] = '';
      $data['inHonor'] = '';
      $data['inMemory'] = '';

      if (isset($this->request->onBehalf) && $this->request->onBehalf)
        $data['onBehalf'] = ' on behalf of ';
      if (isset($this->request->inHonor) && $this->request->inHonor)
        $data['inHonor'] = ' In Honor ';
      if (isset($this->request->inMemory) && $this->request->inMemory)
        $data['inMemory'] = ' In memory of ';

      Mail::send('digitalfox.stripev2::mail.recipient_email', $data, function ($message) {
        $message->subject('Donation Confirmed');
        $message->to($this->request->email);
      });
    } else {
      Mail::send('digitalfox.stripev2::mail.buyer_email', $data, function ($message) {
        $message->subject('Donation Confirmed');
        $message->to($this->request->email);
      });
    }


  }


  private function developmentEmail()
  {

    $developementTeam = DB::table('backend_users')->join('backend_users_groups', 'backend_users_groups.user_id', '=', 'backend_users.id')
      ->join('backend_user_groups', 'backend_user_groups.id', '=', 'backend_users_groups.user_group_id')
      ->where('backend_user_groups.code', 'development')
      ->lists('backend_users.email');

    if (!count($developementTeam)) return;

    $intervalTest = '';

    if ($this->interval == 'month')
      $intervalTest = 'Monthly';

    if ($this->interval == '3-month')
      $intervalTest = 'Quarterly';

    if ($this->interval == 'year')
      $intervalTest = 'Annual';

    $data = [
      'amount' => $this->amount,
      'url' => Config::get('app.url'),
      'date' => date('F d, Y'),
      'day' => date('jS'),
      'monthly' => $intervalTest,
      'donorEmail' => $this->request->email,
      'donorName' => $this->request->first_name . ' ' . $this->request->last_name,
    ];


    foreach ($developementTeam as $team) {

      Mail::send('digitalfox.stripev2::mail.development_email', $data, function ($message) use ($team) {
        $message->subject('Donation Revieved');
        $message->to($team);
      });

    }
  }


  private function getEmailSubscribed()
  {
    try {

      $data = [
        'ca' => Config::get('constantcontact.CONSTANTCONTACT_CA'),
        'list' => Config::get('constantcontact.CONSTANTCONTACT_LIST'),
        'source' => 'EFD',
        'required' => 'list,email',
        'url' => '/newslatter-response',
        'email' => $this->request->email
      ];

      $url = 'https://visitor2.constantcontact.com/api/signup';
      $handle = curl_init($url);
      curl_setopt($handle, CURLOPT_POST, true);
      curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
      $response = curl_exec($handle);
      curl_close($handle);
    } catch (\Exception $e) {
      if ($e->getMessage() != 'Plan already exists.') {
        \Log::info($e->getMessage());
      }
    }
  }

    //
    //
    // private function onHandleSubscription(){
    //
    //   if ($this->request->donation_frequency)
    //   {
    //         \Log::info($this->request);
    //         $this->makePlan();
    //         Session::flash('success-donate', '$'.$this->amount.' '. $this->interval);
    //   }
    //       Session::flash('success-donate', '$'.$this->amount);
    //
    //
    // }
    //

    //
    // private function makeCharge(){
    //   $parm = array(
    //     "amount" =>  $this->amount * 100,
    //     "currency" => "usd",
    //     'source' => $this->request->stripeToken,
    //     "description" => json_encode($this->request)
    //   );
    //
    //   $response =(object) \Stripe\Charge::create($parm)->__toArray(true);
    //
    //   //Make Customer
    //   $this->makeCustomer();
    //
    //   // Store charge
    //   Payment::storeCharge($this->request,$this->amount,$this->customer,$response);
    //
    //   // send mail
    //   $this->buyerEmail();
    //
    //   \Flash::success($this->amount);
    //
    // }
    //
    //

  private function makeToken()
  {
    $cardExprs = explode("/", $this->request->card_expiry);
    $token = (object)\Stripe\Token::create(array(
      "card" => array(
        "number" => $this->request->card_number,
        "exp_month" => $cardExprs[0],
        "exp_year" => $cardExprs[1],
          // "cvc" => $this->request->card_cvc
      )
    ))->__toArray(true);

    return $token->id;

  }

    //

  private function makeCustomer()
  {
    $this->customer = Customer::where('email', $this->request->email)->first();

    if ($this->customer) {
      try {
        $exiestCustomer = (object)\Stripe\Customer::retrieve($this->customer->customer_id)->__toArray(true);
        if (isset($exiestCustomer->deleted) && $exiestCustomer->deleted)
          $this->createCustomer();
        else
          $this->updateCustomer();

      } catch (\Stripe\Error\InvalidRequest $e) {
        $this->createCustomer();
      }
    } else {
      $this->createCustomer();
    }

  }

  private function createCustomer()
  {
    $response = (object)\Stripe\Customer::create(array(
      "description" => "Donater for NSCC",
      "email" => $this->request->email,
        //  "source" => $this->makeToken()
    ))->__toArray(true);

    $this->customer = $this->page['customer'] = Customer::storeCustomer($this->request, $response);
  }


  private function updateCustomer()
  {
    $cu = \Stripe\Customer::retrieve($this->customer->customer_id);
    $cu->description = "Donater of NSCC";
    $cu->source = $this->makeToken(); // obtained with Stripe.js
    $cu->save();


    $this->customer = $this->page['customer'] = Customer::storeCustomer($this->request, $cu);
  }

    //
    // private function makePlan()
    // {
    //   try{
    //     $this->plan = (object) \Stripe\Plan::retrieve('plan-'.$this->amount)->__toArray(true);
    //   } catch(\Stripe\Error\InvalidRequest $e){
    //
    //     $response = (object) \Stripe\Plan::creat\Stripe\Plane(array(
    //         "amount" => $this->amount*100,
    //         "interval" => $this->interval,
    //         "name" => 'plan-'.$this->amount.'-'.$this->interval,
    //         "currency" => "usd",
    //         "trial_period_days" => 30,
    //         "id" => 'plan-'.$this->amount.'-'.$this->interval)
    //     )->__toArray(true);
    //
    //     $this->plan = Plan::storePlan($response);
    //   }
    //
    //  $this->makeSubscription();
    //
    // }
    //
    // private function makeSubscription()
    // {
    //   $this->subscription = Subscription::where('customer_id',$this->customer->id)->first();
    //
    //   if($this->subscription)
    //   {
    //     try{
    //
    //       \Stripe\Subscription::retrieve($this->subscription->subscription_id);
    //       $this->updateSubscription();
    //
    //     } catch(\Stripe\Error\InvalidRequest $e){
    //       $this->createSubscription();
    //     }
    //   }
    //   else{
    //     $this->createSubscription();
    //   }
    //
    // }
    //
    // private function createSubscription(){
    //   $response = (object) \Stripe\Subscription::create(array(
    //     "customer" => $this->customer->customer_id,
    //     "plan" => $this->plan->name
    //   ))->__toArray(true);
    //
    //   Subscription::storeSubscription($this->customer->id,$response);
    // }
    //
    // private function updateSubscription(){
    //
    //   $subscription = \Stripe\Subscription::retrieve($this->subscription->subscription_id);
    //   $subscription->plan = $this->plan->name;
    //   $subscription->save();
    //
    //   Subscription::storeSubscription($this->customer->id,$subscription);
    // }
    //
    //

    //
    //
    // }




}
