<?php namespace Digitalfox\Stripev2;

use Backend;
use System\Classes\PluginBase;

/**
 * stripe Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'stripev2',
            'description' => 'No description provided yet...',
            'author'      => 'digitalfox',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Digitalfox\Stripev2\Components\Stripev2component' => 'Stripev2component',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'digitalfox.stripev2.some_permission' => [
                'tab' => 'DonationsV2',
                'label' => 'Manage DonationsV2'
            ],
        ];
    }

    /**
     * Registers email template items items for this plugin.
     *
     * @return array
     */

    public function registerMailTemplates()
    {
        return [
            'digitalfox.stripev2::mail.buyer_email' => 'Donation confirmation recipient email template.',
            'digitalfox.stripev2::mail.recipient_email' => 'Donation confirmation recipient email template.',
            'digitalfox.stripev2::mail.development_email'  => 'Development team donation recieve email.'
        ];
    }


    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'stripev2' => [
                'label'       => 'DonationsV2',
                'url'         => Backend::url('digitalfox/stripev2/payment'),
                'icon'        => 'icon-usd',
                'permissions' => ['digitalfox.stripev2.*'],
                'order'       => 500,

                'sideMenu' => [
                    'payment' => [
                      'label'       => 'DonationsV2',
                      'url'         => Backend::url('digitalfox/stripev2/payment'),
                      'icon'        => 'icon-usd',
                      'permissions' => ['digitalfox.stripev2.*'],
                    ],
                  ],
            ],
        ];
    }


    public function registerMarkupTags() {
        return [
            'functions' => [
                'config_get' => ['October\Rain\Support\Facades\Config', 'get']
            ]
        ];
    }

}
