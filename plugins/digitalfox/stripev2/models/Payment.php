<?php namespace Digitalfox\Stripev2\Models;

use Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * payment Model
 */
class Payment extends Model
{
  use SoftDeletes;
  /**
   * @var string The database table used by the model.
   */
  public $table = 'digitalfox_stripe';

  /**
   * @var array Guarded fields
   */
  protected $guarded = ['*'];

  /**
   * @var array Fillable fields
   */
  protected $fillable = [];

  /**
   * @var array Relations
   */
  public $hasOne = [];
  public $hasMany = [];
  public $belongsTo = [];
  public $belongsToMany = [];
  public $morphTo = [];
  public $morphOne = [];
  public $morphMany = [];
  public $attachOne = [];
  public $attachMany = [];


  public static function storeCharge($request, $amount, $customer, $response)
  {

    $store = new Payment();
    $store->payment_id = $response->id;
    $store->customer_id = $customer->id;
    $store->name = $request->first_name . ' ' . $request->last_name;
    $store->email = $request->email;
    $store->phone = isset($request->phone) ? $request->phone : "";
    $store->country = isset($request->country) ? $request->country : "";;
    $store->amount = $amount;
    $store->address_1 = $request->address_1;
    $store->address_2 = isset($request->address_2) ? $request->address_2 : "";
    $store->zip = $request->zip;
    $store->city = $request->city;
    $store->state = $request->state;

    if (isset($request->onBehalfHonor)) {
      $store->on_behalf_honor = $request->onBehalfHonor;
      $store->recipient_first_name = isset($request->recipient_first_name) ? $request->recipient_first_name : "";
      $store->recipient_last_name = isset($request->recipient_last_name) ? $request->recipient_last_name : "";
      $store->recipient_email = isset($request->recipient_email) ? $request->recipient_email : "";
    }

    if (isset($request->onBehalf))
      $store->on_behalf = $request->onBehalf;
    if (isset($request->inHonor))
      $store->in_honor = $request->inHonor;
    if (isset($request->inMemory))
      $store->in_memory = $request->inMemory;

    $store->recipient_name = isset($request->recipientName) ? $request->recipientName : "";
    $store->response = json_encode($response);
    $store->save();

  }


  public function getFullAddressAttribute()
  {
    $return = $this->address_1;

    if ($this->address_1)
      $return .= ', ';

    if ($this->address_2)
      $return .= $this->address_2 . ', <br>';

    if ($this->city)
      $return .= '<b>City: </b>' . $this->city . ',<br>';

    if ($this->state)
      $return .= '<b>State: </b>' . $this->state . ',<br>';

    if ($this->zip)
      $return .= '<b>Zip: </b>' . $this->zip;

    return $return;
  }

  public function getDonationTypeAttribute()
  {

    if ($this->on_behalf_honor) {
      $type = 'On behalf or honor of';
      if ($this->recipient_first_name) {
        $type = $type . " " . $this->recipient_first_name;
      }
      if ($this->recipient_last_name) {
        $type = $type . " " . $this->recipient_last_name;
      }
      if ($this->recipient_email) {
        $type = $type . ' (' . $this->recipient_email . ')';
      }
    } else {
      $type = [];
      if ($this->on_behalf)
        $type[] = 'On behalf of';
      if ($this->in_honor)
        $type[] = 'In Honor';
      if ($this->in_memory)
        $type[] = 'In memory of';


      $type = count($type) ? implode(' / ', $type) : 'N/A';

      if ($this->recipient_name)
        $type = $type . ' ' . $this->recipient_name;
    }
    return $type;
  }


  public function getContactInfoAttribute()
  {
    $contact = '';

    if ($this->email)
      $contact .= '<b>Email: </b>' . $this->email . '<br>';

    if ($this->phone)
      $contact .= '<b>Phone: </b>' . $this->phone;


    return $contact;
  }
}
