<?php namespace Digitalfox\Stripev2\Models;

use Model;

/**
 * plan Model
 */
class Plan extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'digitalfox_stripe_plans';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['plan_id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function storePlan($response){
      $store = Plan::firstOrNew(['plan_id'=>$response->id]);
      $store->plan_id = $response->id;
      $store->name = $response->name;
      $store->response = json_encode($response);
      $store->save();

      return $store;
    }

}
