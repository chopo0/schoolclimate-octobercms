<?php namespace Digitalfox\Researchpublication\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddExtraColumnSourceInJournalsTable extends Migration
{

    public function up()
    {
        Schema::table('digitalfox_researchpublication_journals', function($table)
        {
            $table->engine = 'InnoDB';
            $table->text('source');
        });
    }

    public function down()
    {
      Schema::table('digitalfox_researchpublication_journals', function($table)
      {
          $table->engine = 'InnoDB';
          $table->dropColumn('source');
      });
    }

}
