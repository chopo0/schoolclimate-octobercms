  $(document).ready(function() {
    $('body').on('click', '#makepayment', function(){

      $('#paypal-payment-form').attr('id', 'payment-form');
      $('input[name="_handler"]').attr('value', 'Stripecomponent::onHandleForm');
      $('#cardNumber').addClass('required');

      $("#payment-form").validate({
          errorElement: 'p',
          submitHandler: submit,
      });
    });


      function submit(form) {
          // given a valid form, submit the payment details to stripe
          $('#makepayment').attr("disabled", "disabled");
          $('#makepayment').attr('value','Processing Payment...');

          Stripe.createToken({
              number: $('.card-number').val(),
              cvc: $('.card-cvc').val(),
              exp_month: $('.card-expiry-month').val(),
              exp_year: $('.card-expiry-year').val()
          }, function(status, response) {
              if (response.error) {
                  // re-enable the submit button
                  $('#makepayment').removeAttr("disabled")
                  $('#makepayment').attr('value','Submit Donation');

                  DisplayError(response.error.message);

              } else {
                  //Subscribe Newslatter
                  SubscribeNewslatter(response,form);
               }
          });

          return false;
      }

      jQuery.validator.setDefaults({
        errorPlacement: function(error, element) {
          if (element.attr("name") == "donationAmountOther" )
               error.appendTo('.errormessage');
          else
               error.insertAfter(element);
        }
      });

      // add custom rules for credit card validating
      jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
      jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
      jQuery.validator.addMethod("cardExpiry", function() {
          return Stripe.validateExpiry($(".card-expiry-month").val(),
                                       $(".card-expiry-year").val())
      }, "Please enter a valid expiration");

      var select = $(".card-expiry-month"),
          month = new Date().getMonth() + 1;
      for (var i = 1; i <= 12; i++) {
          select.append($("<option value='"+i+"' "+(month === i ? "selected" : "")+">"+i+"</option>"))
      }


      var select = $(".card-expiry-year"),
          year = new Date().getFullYear();
      for (var i = 0; i < 12; i++) {
          select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))
      }
  });

  function DisplayError(message){
     sweetAlert("",message, "error");
  }

  function SubscribeNewslatter(response,form){
    if($('input[name="newslatterSubscribe"]').is(':checked')) {
     $('#newslatter-subscription input[name=email]').attr('value',$('#email').val());
     $.post('https://visitor2.constantcontact.com/api/signup',$('#newslatter-subscription').serialize(),function(data){
         SubmitForm(response,form);
     });
    }
    else SubmitForm(response,form);

  }

  function SubmitForm(response,form){
    // token contains id, last4, and card type
    var token = response['id'];
    // insert the stripe token
    var input = $("<input name='stripeToken' value='" + token + "' style='display:none;' />");
    form.appendChild(input[0]);
    form.submit();
    //$('#RecurringPopup').modal({backdrop: 'static', keyboard: false ,show : true});
  }
