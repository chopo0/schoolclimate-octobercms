<?php namespace Digitalfox\Stripe;

use Backend;
use System\Classes\PluginBase;

/**
 * stripe Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'stripe',
            'description' => 'No description provided yet...',
            'author'      => 'digitalfox',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Digitalfox\Stripe\Components\Stripecomponent' => 'Stripecomponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'digitalfox.stripe.some_permission' => [
                'tab' => 'Donations',
                'label' => 'Manage Donations'
            ],
        ];
    }

    /**
     * Registers email template items items for this plugin.
     *
     * @return array
     */

    public function registerMailTemplates()
    {
        return [
            'digitalfox.stripe::mail.buyer_email' => 'Donation confirmation recipient email template.',
            'digitalfox.stripe::mail.recipient_email' => 'Donation confirmation recipient email template.',
            'digitalfox.stripe::mail.development_email'  => 'Development team donation recieve email.'
        ];
    }


    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'stripe' => [
                'label'       => 'Donations',
                'url'         => Backend::url('digitalfox/stripe/payment'),
                'icon'        => 'icon-usd',
                'permissions' => ['digitalfox.stripe.*'],
                'order'       => 500,

                'sideMenu' => [
                    'payment' => [
                      'label'       => 'Donations',
                      'url'         => Backend::url('digitalfox/stripe/payment'),
                      'icon'        => 'icon-usd',
                      'permissions' => ['digitalfox.stripe.*'],
                    ],
                  ],
            ],
        ];
    }


    public function registerMarkupTags() {
        return [
            'functions' => [
                'config_get' => ['October\Rain\Support\Facades\Config', 'get']
            ]
        ];
    }

}
