<?php namespace Digitalfox\Stripe\Models;

use Model;

/**
 * Subscription Model
 */
class Subscription extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'digitalfox_stripe_subscriptions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['subscription_id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function storeSubscription($customerID,$response){
      $store = Subscription::firstOrNew(['subscription_id'=>$response->id]);
      $store->customer_id = $customerID;
      $store->subscription_id = $response->id;
      $store->response = json_encode($response);
      $store->save();
    }

}
