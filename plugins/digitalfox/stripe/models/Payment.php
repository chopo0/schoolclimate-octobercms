<?php namespace Digitalfox\Stripe\Models;

use Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * payment Model
 */
class Payment extends Model
{
  use SoftDeletes;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'digitalfox_stripe';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public static function storeCharge($request,$amount,$customer,$response){

      $store =  new Payment();
      $store->payment_id = $response->id;
      $store->customer_id = $customer->id;
      $store->name = $request->first_name.' '.$request->last_name;
      $store->email = $request->email;
      $store->phone = $request->phone;
      //$store->country = $request->country;
      $store->amount = $amount;
      $store->address_1 = $request->address_1;
      $store->address_2 = $request->address_2;
      $store->zip = $request->zip;
      $store->city = $request->city;
      $store->state = $request->state;

      if(isset($request->onBehalf))
        $store->on_behalf = $request->onBehalf;
      if(isset($request->inHonor))
        $store->in_honor = $request->inHonor;
      if(isset($request->inMemory))
        $store->in_memory = $request->inMemory;

      $store->recipient_name = $request->recipientName;
      $store->response = json_encode($response);
      $store->save();

    }


    public function getFullAddressAttribute() {
        $return = $this->address_1;

        if($this->address_1)
          $return .= ', ';

        if($this->address_2)
          $return .= $this->address_2.', <br>';

        if($this->city)
          $return .= '<b>City: </b>'.$this->city.',<br>';

        if($this->state)
          $return .= '<b>State: </b>'.$this->state.',<br>';

        if($this->zip)
          $return .= '<b>Zip: </b>'.$this->zip;

        return $return;
    }

    public function getDonationTypeAttribute()
    {
        $type = [];

        if($this->on_behalf)
          $type[] = 'On behalf of';
        if($this->in_honor)
          $type[] = 'In Honor';
        if($this->in_memory)
          $type[] = 'In memory of';


        $type = count($type)?implode(' / ', $type):'N/A';

        if($this->recipient_name)
          $type = $type.' '.$this->recipient_name;

        return $type;
    }


    public function getContactInfoAttribute()
    {
        $contact = '';

        if($this->email)
          $contact .= '<b>Email: </b>'.$this->email.'<br>';

        if($this->phone)
          $contact .= '<b>Phone: </b>'.$this->phone;


        return $contact;
    }
}
