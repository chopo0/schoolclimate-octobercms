<?php namespace Digitalfox\Stripe\Models;

use Model;

/**
 * customer Model
 */
class Customer extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'digitalfox_stripe_customers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['email'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public static function storeCustomer($request,$response){
      $store = Customer::firstOrNew(['email'=>$request->email]);
      $store->customer_id = $response->id;
      $store->email = $request->email;
      $store->response = json_encode($response);
      $store->save();

      return $store;
    }

}
