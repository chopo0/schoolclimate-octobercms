<?php namespace Digitalfox\Stripe\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Digitalfox\Stripe\Models\Payment as PaymentModel;

use ApplicationException;
use Flash;
use Redirect;

/**
 * Payment Back-end Controller
 */
class Payment extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ImportExportController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Digitalfox.Stripe', 'stripe', 'payment');
    }

    public function index()
    {
        $this->vars['totalDonation'] = PaymentModel::all()->sum('amount');

        $this->asExtension('ListController')->index();
    }


    public function export()
    {
      $this->asExtension('ImportExportController')->export();
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $postId) {
                if ((!$payment = PaymentModel::find($postId))) {
                    continue;
                }

                $payment->delete();
            }

            Flash::success('Successfully deleted Books.');
        }

        return $this->listRefresh();
    }

}
