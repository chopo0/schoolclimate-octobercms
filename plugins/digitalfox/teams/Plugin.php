<?php namespace Digitalfox\Teams;

use Backend;
use Backend\Models\User;
use System\Classes\PluginBase;

/**
 * teams Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */

    public $elevated = true;

    public function pluginDetails()
    {
        return [
            'name'        => 'Manage Teams',
            'description' => 'Create team members with details',
            'author'      => 'digitalfox',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
      User::extend(function($model){
        $model->belongsTo['team'] = ['Digitalfox\Teams\Models\Team'];
      });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Digitalfox\Teams\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'digitalfox.teams.some_permission' => [
                'tab' => 'Teams',
                'label' => 'Teams access permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'teams' => [
                'label'       => 'Teams',
                'url'         => Backend::url('digitalfox/teams/teams'),
                'icon'        => 'icon-users',
                'iconSvg'     => 'plugins/digitalfox/teams/assets/images/user-icon.svg',
                'permissions' => ['digitalfox.teams.*'],
                'order'       => 500,
                'sideMenu' => [
                    'teams' => [
                        'label'       => 'Manage Teams',
                        'icon'        => 'icon-users',
                        'url'         => Backend::url('digitalfox/teams/teams'),
                        'permissions' => ['digitalfox.teams.*'],
                    ],
                    'consultants' => [
                        'label'       => 'Manage Education Consultant',
                        'icon'        => 'icon-user-secret',
                        'url'         => Backend::url('digitalfox/teams/consultants'),
                        'permissions' => ['digitalfox.teams.*'],
                    ]
                ]
            ],
        ];
    }
}
