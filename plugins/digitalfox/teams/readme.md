Paste this code to page where you want to use

use digitalfox\teams\models\Team;

function onStart()
{
	$teamArray = [];
	$teams = Team::orderBy('order')->get();


	foreach($teams as $key => $team){
			$teamArray[$key]['name'] = $team->name;
			$teamArray[$key]['email'] = 'mailto:'.$team->email;
			$teamArray[$key]['post'] = $team->post;
			$teamArray[$key]['description'] = $team->description;
			$teamArray[$key]['image_path'] = $team->avatar->getPath();
	}

	$this['our_team'] = $teamArray;
}



