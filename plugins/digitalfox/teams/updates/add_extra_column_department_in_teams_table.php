<?php namespace Digitalfox\Teams\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddExtraColumnDepartmentInTeamsTable extends Migration
{
    public function up()
    {
        Schema::table('digitalfox_teams_teams', function($table) {
            $table->engine = 'InnoDB';
            $table->string('department')->nullable();
        });
    }

    public function down()
    {
        Schema::table('digitalfox_teams_teams', function($table) {
            $table->dropColumn('department');
        });
    }
}
