<?php namespace Digitalfox\Teams\Controllers;

use BackendMenu;
use Backend\Classes\Controller;


use ApplicationException;
use Flash;
use Redirect;

use Digitalfox\Teams\Models\Consultant;

/**
 * Consultants Back-end Controller
 */
class Consultants extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Digitalfox.Teams', 'teams', 'consultants');
    }

    public function index()
    {
        $this->addJs('/plugins/digitalfox/teams/assets/js/post-form.js');
        $this->asExtension('ListController')->index();
    }


    public function create()
    {
        BackendMenu::setContextSideMenu('consultants');

        $this->addJs('/plugins/digitalfox/teams/assets/js/post-form.js');

        return $this->asExtension('FormController')->create();
    }


    public function update($recordId = null)
    {
        return $this->asExtension('FormController')->update($recordId);
    }



    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $postId) {
                if ((!$post = Consultant::find($postId))) {
                    continue;
                }

                $post->delete();
            }

            Flash::success('Successfully deleted those Consultants.');
        }

        return $this->listRefresh();
    }
}
