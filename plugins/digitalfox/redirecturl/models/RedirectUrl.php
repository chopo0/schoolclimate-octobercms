<?php namespace Digitalfox\Redirecturl\Models;

use Model;

/**
 * RedirectUrl Model
 */
class RedirectUrl extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'digitalfox_redirecturl_redirect_urls';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


  

}
