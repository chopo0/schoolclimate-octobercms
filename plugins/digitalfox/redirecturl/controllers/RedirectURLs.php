<?php namespace Digitalfox\Redirecturl\Controllers;

use BackendMenu;
use Backend\Classes\Controller;


use ApplicationException;
use Flash;
use Redirect;

use Digitalfox\Redirecturl\Models\RedirectUrl;

/**
 * Redirect U R Ls Back-end Controller
 */
class RedirectURLs extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Digitalfox.Redirecturl', 'redirecturl', 'redirecturls');
    }

    public function index()
    {

        $this->asExtension('ListController')->index();
    }


    public function create()
    {
        BackendMenu::setContextSideMenu('redirecturls');
        return $this->asExtension('FormController')->create();
    }


    public function update($recordId = null)
    {
        return $this->asExtension('FormController')->update($recordId);
    }



    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $postId) {
                if ((!$post = RedirectUrl::find($postId))) {
                    continue;
                }

                $post->delete();
            }

            Flash::success('Successfully deleted Books.');
        }

        return $this->listRefresh();
    }

    public function index_onActive()
    {
      if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

          foreach ($checkedIds as $postId) {
              if ((!$post = RedirectUrl::find($postId))) {
                  continue;
              }
              $post->active = 'Yes';
              $post->save();
          }

          Flash::success('Successfully Activated.');
      }

      return $this->listRefresh();
    }

    public function index_onInactive()
    {
      if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

          foreach ($checkedIds as $postId) {
              if ((!$post = RedirectUrl::find($postId))) {
                  continue;
              }
              $post->active = 'No';
              $post->save();
          }

          Flash::success('Successfully Inactivated.');
      }

      return $this->listRefresh();
    }
}
