<?php namespace Digitalfox\Redirecturl;

use Backend;
use System\Classes\PluginBase;

/**
 * redirecturl Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Redirect URLs',
            'description' => 'Manage broken URLs',
            'author'      => 'digitalfox',
            'icon'        => 'icon-link'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Digitalfox\Redirecturl\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'digitalfox.redirecturl.some_permission' => [
                'tab' => 'redirecturl',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'redirecturl' => [
                'label'       => 'Redirect URL',
                'url'         => Backend::url('digitalfox/redirecturl/redirecturls'),
                'icon'        => 'icon-link',
                'permissions' => ['digitalfox.redirecturl.*'],
                'order'       => 500,
                'sideMenu' => [
                    'redirecturls' => [
                        'label'       => 'Manage Redirect URLs',
                        'icon'        => 'icon-link',
                        'url'         => Backend::url('digitalfox/redirecturl/redirecturls'),
                        'permissions' => ['digitalfox.redirecturl.*'],
                    ],
                ]
            ],
        ];
    }

}
