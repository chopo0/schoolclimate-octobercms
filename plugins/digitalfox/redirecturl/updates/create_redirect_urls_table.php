<?php namespace Digitalfox\Redirecturl\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateRedirectUrlsTable extends Migration
{

    public function up()
    {
        Schema::create('digitalfox_redirecturl_redirect_urls', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('active_url');
            $table->string('current_url');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('digitalfox_redirecturl_redirect_urls');
    }

}
