<?php namespace Digitalfox\Redirecturl\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddFieldActiveInRedirectUrlsTable extends Migration
{

    public function up()
    {
        Schema::table('digitalfox_redirecturl_redirect_urls', function($table)
        {
          $table->string('active')->after('current_url');
        });
    }

    public function down()
    {
      Schema::table('digitalfox_redirecturl_redirect_urls', function($table)
      {
          $table->dropColumn('active');
      });

    }

}
