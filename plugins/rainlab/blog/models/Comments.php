<?php namespace Rainlab\Blog\Models;

use Model;

/**
 * comments Model
 */
class Comments extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'rainlab_blog_comments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['post' => ['RainLab\Blog\Models\Post']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public function store(array $request){

         $this->post_id = $request['post_id'];
         $this->name    = $request['name'];
         $this->email   = $request['email'];
         $this->web_url = $request['web_url'];
         $this->comment = $request['comment'];
         $this->save();

         \Flash::success('Successfully Submited!');
    }

}
