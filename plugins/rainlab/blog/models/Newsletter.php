<?php namespace RainLab\Blog\Models;

use App;
use Str;
use Html;
use Lang;
use Model;
use Markdown;
use ValidationException;
use RainLab\Blog\Classes\TagProcessor;
use Backend\Models\User;
use Carbon\Carbon;
use DB;

class Newsletter extends Model
{
  use \October\Rain\Database\Traits\Validation;

    public $table = 'news_latters';

    protected $guarded = [];

    protected $dates = ['published_at'];

    public $rules = [
        'name' => 'required',
        'news_latter_image' => 'required',
        'published_at' => 'required',
        'slug' => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:news_latters'],

    ];

    public static $allowedSortingOptions = array(
        'title asc' => 'Title (ascending)',
        'title desc' => 'Title (descending)',
        'created_at asc' => 'Created (ascending)',
        'created_at desc' => 'Created (descending)',
        'updated_at asc' => 'Updated (ascending)',
        'updated_at desc' => 'Updated (descending)',
        'published_at asc' => 'Published (ascending)',
        'published_at desc' => 'Published (descending)',
        'random' => 'Random'
    );

  public function getChildren(){

  }

  public function getChildCount(){

  }

  public function getAllRoot(){
     return Newsletter::all();
  }


  public $attachMany = [
      'news_letter_image' => ['System\Models\File', 'order' => 'sort_order'],
      'content_images' => ['System\Models\File']
  ];

  public function scopeIsPublished($query)
  {
      return $query
          ->whereNotNull('published_at')
          ->where('published_at', '<', Carbon::now())
      ;
  }



  /**
   * Lists posts for the front end
   * @param  array $options Display options
   * @return self
   */
  public function scopeListFrontEnd($query, $options)
  {
      /*
       * Default options
       */
      extract(array_merge([
          'page'       => 1,
          'perPage'    => 30,
          'sort'       => 'created_at',
          'categories' => null,
          'category'   => null,
          'search'     => '',
          'published'  => true
      ], $options));

      $searchableFields = ['title', 'slug', 'excerpt', 'content'];

      if ($published) {
          $query->isPublished();
      }

      /*
       * Sorting
       */
      if (!is_array($sort)) {
          $sort = [$sort];
      }

      foreach ($sort as $_sort) {

          if (in_array($_sort, array_keys(self::$allowedSortingOptions))) {
              $parts = explode(' ', $_sort);
              if (count($parts) < 2) {
                  array_push($parts, 'desc');
              }
              list($sortField, $sortDirection) = $parts;
              if ($sortField == 'random') {
                  $sortField = DB::raw('RAND()');
              }
              $query->orderBy($sortField, $sortDirection);
          }
      }

      /*
       * Search
       */
      $search = trim($search);
      if (strlen($search)) {
          $query->searchWhere($search, $searchableFields);
      }


      return $query->paginate($perPage, $page);
  }



}
