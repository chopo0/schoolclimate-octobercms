<?php namespace RainLab\Blog\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use RainLab\Blog\Models\Newsletter as NewsletterModel;
use RainLab\Blog\Models\Post as PostModel;

class Newsletter extends ComponentBase
{
    /**
     * @var RainLab\Blog\Models\Post The post model used for display.
     */
    public $post;

    /**
     * @var string Reference to the page name for linking to categories.
     */
    public $categoryPage;

    public $posts;

    public function componentDetails()
    {
        return [
            'name'        => 'rainlab.blog::lang.settings.news_latter_tittle',
            'description' => 'rainlab.blog::lang.settings.news_latter_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'rainlab.blog::lang.settings.post_slug',
                'description' => 'rainlab.blog::lang.settings.post_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->post = $this->page['newsLetter'] = $this->loadPost();

    }

    protected function loadPost()
    {
        $slug = $this->property('slug');
        $newsLatter = NewsletterModel::where('slug', $slug)->first();
        $newsLatter['content'] = PostModel::formatHtml($newsLatter->news_latter, true);
        $newsLatter['date'] = date('F, d, Y', strtotime($newsLatter->published_at->toDateTimeString()));

        return $newsLatter;
    }
}
