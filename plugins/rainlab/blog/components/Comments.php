<?php namespace Rainlab\Blog\Components;
use Request;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use RainLab\Blog\Models\Post as BlogPost;
use RainLab\Blog\Models\Comments as PostComments;

class Comments extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'comments Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [

          'slug' => [
              'title'       => 'rainlab.blog::lang.settings.post_slug',
              'description' => 'rainlab.blog::lang.settings.post_slug_description',
              'default'     => '{{ :slug }}',
              'type'        => 'string'
          ]

        ];
    }

    public function onRun()
    {
       $this->page['post'] = BlogPost::isPublished()->where('slug', $this->property('slug'))->first();;
       $this->page['comments'] = PostComments::where('post_id',$this->page['post']->id)->orderBy('id','DESC')->paginate(10);

    }


    public function onHandleForm(){
      $model = new PostComments();
      $model->store(Request::all());
    }



}
