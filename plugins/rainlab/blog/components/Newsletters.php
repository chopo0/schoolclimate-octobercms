<?php namespace RainLab\Blog\Components;

use Redirect;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use RainLab\Blog\Models\Post as PostModel;

use RainLab\Blog\Models\Category as BlogCategory;
use RainLab\Blog\Models\Newsletter as NewNewsletter;

class NewsLetters extends ComponentBase
{

    /**
     * Parameter to use for the page number
     * @var string
     */
    public $pageParam;


    /**
     * Message to display when there are no messages.
     * @var string
     */
    public $noNewsLettersMessage;


    /**
     * If the post list should be ordered by another attribute.
     * @var string
     */
    public $sortOrder;


    public $sideMenuPosts;

    /*
       A Collection of News Letters
    */
    public $newsletters;


    public function componentDetails()
    {
        return [
            'name'        => 'rainlab.blog::lang.settings.news_latters_tittle',
            'description' => 'rainlab.blog::lang.settings.news_latters_description'
        ];
    }

    public function defineProperties()
    {

        return [
            'pageNumber' => [
                'title'       => 'rainlab.blog::lang.settings.posts_pagination',
                'description' => 'rainlab.blog::lang.settings.posts_pagination_description',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'postsPerPage' => [
                'title'             => 'rainlab.blog::lang.settings.posts_per_page',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'rainlab.blog::lang.settings.posts_per_page_validation',
                'default'           => '10',
            ],
            'noNewsLettersMessage' => [
                'title'        => 'rainlab.blog::lang.settings.posts_no_posts',
                'description'  => 'rainlab.blog::lang.settings.posts_no_posts_description',
                'type'         => 'string',
                'default'      => 'No news letters found',
                'showExternalParam' => false
            ],
            'sortOrder' => [
                'title'       => 'rainlab.blog::lang.settings.posts_order',
                'description' => 'rainlab.blog::lang.settings.posts_order_description',
                'type'        => 'dropdown',
                'default'     => 'published_at desc'
            ],

        ];
    }



    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getPostPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getSortOrderOptions()
    {
        return NewNewsletter::$allowedSortingOptions;
    }



    public function onPartialPagination()
    {
      $this->prepareVars();
      $this->newsletters = $this->page['newsletters'] = $this->newslettrs();
    }



    public function onRun()
    {

        $this->prepareVars();

        $this->newsletters = $this->page['newsletters'] = $this->newslettrs();


        /*
         * If the page number is not valid, redirect
         */
        if ($pageNumberParam = $this->paramName('pageNumber')) {
            $currentPage = $this->property('pageNumber');

            if ($currentPage > ($lastPage = $this->newsletters->lastPage()) && $currentPage > 1)
                return Redirect::to($this->currentPageUrl([$pageNumberParam => $lastPage]));
        }
    }


    protected function newslettrs()
    {

        /*
         * List all the posts, eager load their categories
         */
        $newsletters = NewNewsletter::orderBy('published_at', 'desc')->paginate(9);


         $newsletters->each(function($newsletter) {
            $newsletter['content'] = PostModel::formatHtml($newsletter->news_latter, true);
            $newsletter['date'] = date('F, d, Y', strtotime($newsletter->published_at->toDateTimeString()));

         });

        return $newsletters;
    }


    protected function prepareVars()
    {
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->noNewsLettersMessage = $this->page['noNewsLettersMessage'] = $this->property('noNewsLettersMessage');

    }

    protected function listNewsLetters($search=null)
    {

        /*
         * List all the posts, eager load their categories
         */
        $newsletters =new NewNewsletter;

                       $newsletters = $newsletters->orderBy('id','desc')->listFrontEnd([
                           'page'       => $this->property('pageNumber'),
                           'sort'       => $this->property('sortOrder'),
                           'perPage'    => $this->property('postsPerPage'),
                       ]);


         $newsletters->each(function($newsletter) {
            $newsletter['content'] = PostModel::formatHtml($newsletter->news_latter, true);
            $newsletter['date'] = date('F, d, Y', strtotime($newsletter->published_at->toDateTimeString()));

         });

        return $newsletters;
    }


    }
